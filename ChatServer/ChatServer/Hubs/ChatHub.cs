using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatServer.Utils;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace ChatServer.Hubs
{
    public class ChatHub : Hub
    {
        private static Dictionary<string, string> _activeUsers = new(); //Used to track all active users.

        /// <summary>
        /// Informs all connected users with a new text message
        /// </summary>
        /// <param name="user">The senders name</param>
        /// <param name="message">The message</param>
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        /// <summary>
        /// Registers a new user.
        /// </summary>
        /// <param name="request">The login request with username and connectionId</param>
        /// <returns>If the login was successful</returns>
        public async Task<bool> Register(LoginRequest request)
        {
            if (_activeUsers.ContainsKey(request.Username))
            {
                Console.WriteLine("User " + request.Username +
                                  " tried to login, but failed because username is already in use!");
                return await Task.FromResult(false);
            }

            Console.WriteLine("Successfully registered username: " + request.Username);
            await Clients.Client(request.ConnectionId).SendAsync("ReceiveMessage", "Server", "You are now connected!");
            _activeUsers.Add(request.Username, request.ConnectionId);

            // Send active users
            var data = JsonConvert.SerializeObject(new UsersResponse(_activeUsers.Keys.ToList()));
            await Clients.All.SendAsync("GetAvailableUsers", data);

            return await Task.FromResult(true);
        }

        /// <summary>
        /// Default method for disconnected users.
        /// Deletes the disconnected user from the active list.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public override Task OnDisconnectedAsync(Exception? exception)
        {
            var user = _activeUsers.SingleOrDefault(x => x.Value == Context.ConnectionId);
            _activeUsers.Remove(user.Key);

            Console.WriteLine("Successfully disconnected username: " + user.Key);

            var data = JsonConvert.SerializeObject(new UsersResponse(_activeUsers.Keys.ToList()));
            Clients.All.SendAsync("GetAvailableUsers", data);

            return base.OnDisconnectedAsync(exception);
        }
    }
}