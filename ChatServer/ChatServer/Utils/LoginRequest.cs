namespace ChatServer.Utils
{
    /// <summary>
    /// Used to login to the signalr server with username and the connectionId.
    /// The connectionId is obsolete because the server knows which client is trying to login but for consistency it is encapsulated in one request object
    /// </summary>
    public class LoginRequest
    {
        public string Username { get; }
        public string ConnectionId { get; }

        public LoginRequest(string username, string connectionId)
        {
            Username = username;
            ConnectionId = connectionId;
        }
    }
}