using System.Collections.Generic;

namespace ChatServer.Utils
{
    /// <summary>
    /// The response object you get when asking for active users
    /// </summary>
    public class UsersResponse
    {
        public List<string> Usernames { get; }
        public string ErrorMessage { get; }

        public UsersResponse(List<string> usernames)
        {
            Usernames = usernames;
            ErrorMessage = "";
        }

        public UsersResponse(List<string> usernames, string errorMessage)
        {
            Usernames = usernames;
            ErrorMessage = errorMessage;
        }
    }
}