namespace ChatClientWPF.Utils
{
    /// <summary>
    /// Enum like class in order to minimize the possibility of misspelling 
    /// </summary>
    public static class HubConnections
    {
        public const string Receive = "ReceiveMessage";
        public const string Register = "Register";
        public const string Send = "SendMessage";
        public const string GetAvailableUsers = "GetAvailableUsers";
    }
}