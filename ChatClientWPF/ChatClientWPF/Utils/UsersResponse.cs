using System.Collections.Generic;

namespace ChatServer.Utils
{
    /// <summary>
    /// The response object you get when asking for active users
    /// </summary>
    public class UsersResponse
    {
        public List<string> Usernames { get; set; }
        public string ErrorMessage { get; set; }
    }
}