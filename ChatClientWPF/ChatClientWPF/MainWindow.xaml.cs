﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using ChatClientWPF.Annotations;
using ChatClientWPF.Utils;
using ChatServer.Utils;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;

namespace ChatClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HubConnection _connection; //The connection object
        private const string HubEndpoint = "/ChatHub"; //SignalR's endpoint
        private string _username = ""; //Your username


        public string DefaultAddress { get; set; }

        public MainWindow()
        {
            DefaultAddress = "http://localhost:5000";
            InitializeComponent();
            Username.Text = "JackSparrow" + new Random().Next(3000);
        }

        /// <summary>
        /// All events used by the server to communicate with its clients.
        /// </summary>
        private void RegisterEvents()
        {
            _connection.On<string, string>(HubConnections.Receive,
                (user, message) =>
                {
                    ChatHistory.Text += "\n";
                    ChatHistory.Text += "Pirate: " + user + " \"" + message + "\"";
                });

            _connection.On<string>(HubConnections.GetAvailableUsers,
                (data) =>
                {
                    var usersResponse = JsonConvert.DeserializeObject<UsersResponse>(data);
                    if (usersResponse?.Usernames != null)
                    {
                        ConnectedUsersList.Items.Clear();
                        foreach (var responseUsername in usersResponse?.Usernames)
                        {
                            ConnectedUsersList.Items.Add(responseUsername == _username
                                ? responseUsername + " It`s you "
                                : responseUsername);
                        }
                    }
                });
        }

        /// <summary>
        /// Disconnects from the server if a connection is established
        /// </summary>
        private async Task Disconnect()
        {
            if (_connection is {State: HubConnectionState.Connected})
            {
                ChatHistory.Text += "\n";
                ChatHistory.Text += "Disconnecting .....";
                await _connection.StopAsync();
                ConnectionButton.IsEnabled = true;
                ServerAddress.IsEnabled = true;
                Username.IsEnabled = true;
                ConnectedUsersList.Items.Clear();
            }
        }

        #region ButtonEvents

        /// <summary>
        /// Connnects to the signalR server and logs you in.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Connect_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                ChatHistory.Text += "\n";
                ChatHistory.Text += "Sailing to the server...";

                ConnectionButton.IsEnabled = false;
                if (Username.Text == "")
                {
                    MessageBox.Show("You got to give yourself a name PIRATE!");
                    return;
                }

                _username = Username.Text;

                _connection = new HubConnectionBuilder()
                    .WithUrl(DefaultAddress + HubEndpoint)
                    .WithAutomaticReconnect()
                    .Build();

                await _connection.StartAsync();

                ConnectionButton.IsEnabled = false;

                RegisterEvents();

                ChatHistory.Text += "\n";
                ChatHistory.Text += "Hiring sailors...";
                var isOk =
                    await _connection.InvokeAsync<bool>(HubConnections.Register,
                        new LoginRequest(_username, _connection.ConnectionId));

                if (!isOk)
                {
                    MessageBox.Show("Pirate name already in use..");
                    await _connection.StopAsync();
                    ConnectionButton.IsEnabled = true;
                }


                ServerAddress.IsEnabled = false;
                Username.IsEnabled = false;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                ConnectionButton.IsEnabled = true;
                MessageBox.Show("The Black Pearl has fallen!");
            }
        }

        /// <summary>
        /// Logs you out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void DisconnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            await Disconnect();
        }

        /// <summary>
        /// Closes the app and logs you out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void CloseButton_OnClick(object sender, RoutedEventArgs e)
        {
            await Disconnect();
            Close();
        }

        /// <summary>
        /// Sends the text message to the signalR server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SendButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_connection is {State: HubConnectionState.Connected})
            {
                await _connection.InvokeAsync(HubConnections.Send, _username, MessageTextBox.Text);
            }
        }

        #endregion
    }
}