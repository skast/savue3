﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace ChatClient
{
    class Program
    {
        public static List<string> AvailableUsers;

        private static async Task Main(string[] args)
        {
            var username = "MarkBoy69";


            var connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/ChatHub")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();

            RegisterEvents(connection);

            var isOk =
                connection.InvokeAsync<bool>(HubConnections.Register,
                    new LoginRequest(username, connection.ConnectionId)).Result;

            if (!isOk)
            {
                Console.WriteLine("Username already in use!");
                return;
            }

            isOk = OtherUsersAvailable(connection);

            if (isOk)
            {
                Console.WriteLine("You are ready to chat");
                Console.WriteLine("To kill the session enter 'x'");
                var message = "";
                while ((message = Console.ReadLine()) != "x")
                {
                    await connection.InvokeAsync(HubConnections.Send, "MarkBoy69", message);
                }
            }
        }

        private static bool OtherUsersAvailable(HubConnection connection)
        {
            var response = connection
                .InvokeAsync<UsersResponse>(HubConnections.GetAvailableUsers, connection.ConnectionId)
                .Result;

            if (response.Usernames != null)
            {
                AvailableUsers = response.Usernames;
                return true;
            }

            Console.WriteLine("You are not able to request the available users!");
            return false;
        }

        private static void RegisterEvents(HubConnection connection)
        {
            connection.On<string, string>(HubConnections.Receive,
                (user, message) => { Console.WriteLine(user + ": " + message); });
        }
    }
}