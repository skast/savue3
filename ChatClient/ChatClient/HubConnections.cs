namespace ChatClient
{
    public static class HubConnections
    {
        public const string Receive = "ReceiveMessage";
        public const string Register = "Register";
        public const string Send = "SendMessage";
        public const string GetAvailableUsers = "GetAvailableUsers";
    }
}