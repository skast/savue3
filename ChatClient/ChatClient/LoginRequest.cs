namespace ChatClient
{
    public class LoginRequest
    {
        public string Username { get; }
        public string ConnectionId { get; }

        public LoginRequest(string username, string connectionId)
        {
            Username = username;
            ConnectionId = connectionId;
        }
    }
}